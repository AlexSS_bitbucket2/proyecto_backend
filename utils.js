function validarEmail (valor) {
 expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
 var estado =false;
  if ( !expr.test(valor) ) {
      console.log ("Error: La dirección de email " + valor + " es incorrecta.");

        }else
            {
              console.log ("Email correcto");
              estado =true;
             }
  return estado ;
}


function validarImporte (cifra){
  exprnum =/^[0-9]+([.][0-9]{1,2})?$/;
  var estadonum =false;
  if (!exprnum.exec(cifra)){
    console.log ("El importe "+ cifra + " no es correcto");
  } else {
    console.log ("El importe "+ cifra + " es correcto")
    estadonum =true;
  }
  return estadonum;
}


module.exports.validarEmail = validarEmail;
module.exports.validarImporte = validarImporte;
