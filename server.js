//carga los valores con la configuración por defecto
require('dotenv').config();

const express = require('express');
const app = express();
//const {sanitizeBody} = require('express-validator/filter');

//define el puerto por el que escuchar, y sin lo asigna al 3000
const port = process.env.PORT || 3000;

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");
 next();
}

app.use(express.json());
app.use(enableCORS);
app.use(function(err, req, res, next) {
  if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
// Handle the error here
    console.log('Bad JSON');
    res.status(500).send(`JSON con errores de sintaxis: ${err.type} `);
      }
      else{
      next();
    }
// Pass the error to the next middleware if it wasn't a JSON parse error
  }
);


//Definir variables para utilizar las funciones del controller
const authController = require('./controllers/AuthController');
const userController = require('./controllers/UserController');
const accountController = require('./controllers/AccountController');
const moveController =require('./controllers/MoveController');
const listmController =require('./controllers/ListMController');



app.listen(port);
console.log("API escuchando en el puerto:" + port);

//login
app.post('/apitdm/login', authController.postLogin);
//logout
app.post('/apitdm/logout/:id', authController.postLogout);
//obtener lista usuarios
app.get('/apitdm/usuarios', userController.getUsuarios);
//obtener 1 usuario
app.get('/apitdm/usuarios/:id', userController.getUsuarioById);
//alta nuevo usuario
app.post('/apitdm/usuarios', userController.postcrearUsuario);
//borrar 1 usuario
app.delete('/apitdm/usuarios/', userController.deleteUsuario);
//Crear cuenta
//app.post('/apitdm/cuentas', userController.postcrearCuenta);

app.post('/apitdm/cambios/',userController.changeUsuario);


//Obtener lista de cuentas
app.get('/apitdm/cuentas/', accountController.getAccount);
//Listar cuentas de un usuario
app.get('/apitdm/cuentas/:id', accountController.getAccountById);
//Crear una cuenta
app.post('/apitdm/cuentas/', accountController.postAccount);

//borrar 1 cuenta
app.delete('/apitdm/cuentas/', accountController.deleteAccountByIban);

//Ingresar dinero
app.post('/apitdm/ingresos', moveController.postIngreso);
//app.post('/apitdm/ingresos', sanitizeBody('importe').toFloat, moveController.postIngreso);

//Sacar dinero (reintegros)
app.post('/apitdm/reintegros',moveController.postReintegro);
//Listar los movimientos de todos los usuarios
app.get('/apitdm/movimientos/', listmController.getOperations);
//Listar movimientos de una cuenta
app.get('/apitdm/movimientosIban/:iban', listmController.getOperationsByIBAN);
