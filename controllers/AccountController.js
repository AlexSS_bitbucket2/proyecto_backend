const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitdmdb/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_APY_KEY;
const crypt = require('../crypt');
const utils =require('../utils');


//Listar todas las cuentas dadas de alta
function getAccount (req,res) {

  console.log("GET /apitdm/cuentas/");
  var httpClient = requestJson.createClient (baseMLabURL);

  httpClient.get("cuentas?" + mLabAPIKey,
   function(err, resMLab, body){
     var response = !err ? body : {
       "msg" : "Error obteniendo cuentas"
     }
     res.send(response);
   }
 )
}

function getAccountById(req,res){
  console.log("GET /apitdm/cuentas/:id");
  var httpClient = requestJson.createClient (baseMLabURL);
  var id = req.params.id;
  var query='q={"idUsuario":' + id +'}';

  httpClient.get ("cuentas?" + query + "&" + mLabAPIKey,
      function(err,resMLab,body){


        if (err){
          var response= {
            "msg": "Error obteniendo cuentas"
          }
          res.status(500);
        } else {
          if (body.length > 0){
            var response = body;
          } else {
            var response ={
             "msg": "Usuario no tiene cuenta asociada"
          }
            res.status(404);
          }
        }
     res.send(response);

      }
  )
}

function postAccount (req,res){
  console.log("POST /apitdm/accounts/")
  var valor = req.body.email;

  if (utils.validarEmail (valor)){
    var httpClient = requestJson.createClient(baseMLabURL);
    var query ="f={'id':1 ,'email':1,'_id':0}&";

    //comprobar que existe usuario
    httpClient.get("usuarios?" + query + mLabAPIKey,
        function(err, resMLab, body){
            var response = !err ? body : {
            "msg" : "Error obteniendo email"
          }

          var findEmail =false;

          for (var i=0; i < body.length ;i++){

            if(body[i].email==req.body.email){

              var idUsuario = body[i].id;

              findEmail =true;
              break;
            }
          }

          if (findEmail==true){

            var numAccount = req.body.iban;

            var httpClient = requestJson.createClient (baseMLabURL);

            var query ="f={'idUsuario':1 ,'IBAN':1,'_id':0}&";

            httpClient.get("cuentas?" + query + mLabAPIKey,
                function(errAccount, resMLabAccount, bodyAccount){
                  var response = !errAccount ? bodyAccount : {
                    "msg" : "Error comprobando si la cuenta existe"
                  }

                  var findAccount =false;
                    for (var i=0; i < bodyAccount.length ;i++){

                       if(bodyAccount[i].IBAN==numAccount){
                          findAccount = true;
                          break;
                          }
                        }

                      if (findAccount==false) {
                        var newAccount = {
                          "idUsuario": idUsuario,
                          "IBAN": numAccount,
                          "balance": 0
                        }

                        var httpClient = requestJson.createClient(baseMLabURL);
                        httpClient.post("cuentas?" + mLabAPIKey, newAccount,
                          function(err,resMlab,body) {

                            res.status(201).send ({msg:"Cuenta creada", "id":idUsuario});
                            }

                        )

                      }

                }
              )
            }
          }

        )
      } else {
        res.status (500).send ({"msg": "Usuario no valido"});
      }
  }

  function deleteAccountByIban (req,res){
    console.log("DELETE /apitdm/cuentas/");
    console.log (req.body.IBAN);
    var numAccount = req.body.IBAN;
    var httpClient = requestJson.createClient(baseMLabURL);

    httpClient.get("cuentas?"+ mLabAPIKey,
    function(errAccount, resMLabAccount, bodyAccount){
      var response = !errAccount ? bodyAccount : {
        "msg" : "Error obteniendo cuentas"
        }
      console.log (bodyAccount.length);
      var findAccount =false;

      for (var i=0; i < bodyAccount.length ;i++){

       console.log ("hola " + i);
       console.log (bodyAccount[i].IBAN + "  " + req.body.IBAN);

        if(bodyAccount[i].IBAN==req.body.IBAN){

          var idMongoAccount=bodyAccount[i]._id.$oid;
          findAccount =true;
          break;
          }
         } //fin del for


        if (findAccount==true){

          console.log ("comprobar si tiene movimientos");
          var query='q={"IBAN":' +'"' +numAccount +'"}';
          console.log(query);

          httpClient.get ("movimientos?" + query + "&" + mLabAPIKey,
              function(errMovimientos,resMLabMovimientos,bodyMovimientos){
                console.log ("he accedido a mi coleccion movimientos")

                console.log("Error: " + resMLabMovimientos.statusCode);
                console.log (bodyMovimientos.length);
                if (errMovimientos){
                  var response= {
                    "msg": "Error obteniendo movimientos"
                  }
                  res.status (500).send ({"msg": "Error obteniendo movimientos"});
                } else {

                    if ( bodyMovimientos.length > 0){
                      console.log ("hay movimientos");
//empieza el borrado de movimientos
                        for (i=0; i < bodyMovimientos.length; i++){

                          var idMongoMoves = bodyMovimientos[i]._id.$oid;
                          console.log(idMongoMoves);

                            var httpClient = requestJson.createClient(baseMLabURL);
                            httpClient.delete("movimientos/" + idMongoMoves + "?" + mLabAPIKey,
                              function(errDelMoves, resMLabDelMoves, bodyDelMoves) {
                                  console.log("err" + resMLabDelMoves.statusCode);
                                  if (errDelMoves) {
                                      var response = {
                                        "msg" : "Error borrando movimientos"
                                        }
                                        res.status(500).send ({msg:"Error borrando cuenta","IBAN": numAccount});

                                      } else {

                                        console.log ("movimiento borrado");
                                  //res.status(200).send ({msg:"Movimiento borrado","IBAN": numAccount});
                                    }
                                  }
                                )

                        }//fin del for de movimientos


                          var httpClient = requestJson.createClient(baseMLabURL);
                          httpClient.delete("cuentas/" + idMongoAccount + "?" + mLabAPIKey,
                            function(errDel, resMLabDel, bodyDel) {

                                if (errDel) {
                                    var response = {
                                      "msg" : "Error borrando usuario"
                                      }
                                  res.status(500).send ({msg:"Error borrando cuenta","IBAN": numAccount});
                                    } else {

                                res.status(200).send ({msg:"Cuenta Borrada","IBAN": numAccount});
                                  }
                                }
                              )

//termina borrado de movimientos
                    }

                    else {

                      console.log ("no hay moves y borro cuenta");

                      console.log (idMongoAccount);

                      var httpClient = requestJson.createClient(baseMLabURL);
                      httpClient.delete("cuentas/" + idMongoAccount + "?" + mLabAPIKey,
                        function(errDel, resMLabDel, bodyDel) {

                            if (errDel) {
                                var response = {
                                  "msg" : "Error borrando usuario"
                                  }
                              res.status(500).send ({msg:"Error borrando cuenta","IBAN": numAccount});
                                } else {

                            res.status(200).send ({msg:"Cuenta Borrada","IBAN": numAccount});
                              }
                            }
                          )
                        }

                      }
                    }
                  )
                } else { //cierre findAccount==true
                  console.log("Número de cuenta no existe");
                  res.send({"msg": "Cuenta no existe", "IBAN" :req.body.IBAN});
            }
        }
    )
  }

module.exports.getAccount = getAccount;
module.exports.getAccountById = getAccountById;
module.exports.postAccount = postAccount;
module.exports.deleteAccountByIban = deleteAccountByIban;
