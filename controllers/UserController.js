//const io = require('../io');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitdmdb/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_APY_KEY;
const crypt = require('../crypt');
const utils =require('../utils');

//obtener lista usuarios
function getUsuarios (req, res) {
  console.log("GET /apitdm/usuarios");

  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("usuarios?" + mLabAPIKey,
    function(err, resMLab, body){
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }
  )
}

//obtener 1 usuario
function getUsuarioById(req, res) {
  console.log("GET /apitdm/usuarios/:id");

  var httpClient = requestJson.createClient(baseMLabURL);

  var query = "q={'id':" + req.params.id + "}";


  httpClient.get("usuarios?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body[0];
        } else {
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}
//crear usuario
function postcrearUsuario(req, res) {

  console.log("POST /apitdm/usuarios");

  var valor = req.body.email;
  if (utils.validarEmail (valor)){

    var httpClient = requestJson.createClient(baseMLabURL);
    var query ="f={'id':1 ,'email':1,'_id':0}&";

    //comprobar no existe usuario
    httpClient.get("usuarios?" + query + mLabAPIKey,
        function(err, resMLab, body){
          var response = !err ? body : {
            "msg" : "Error obteniendo email"
          }

          var findEmail =false;

          for (var i=0; i < body.length ;i++){

            if(body[i].email==req.body.email){

              res.send({"msg": "Usuario ya existe.", ",mail" : body[i].mail});

              findEmail =true;
              break;
            }
          }

          if (findEmail == false){

              var httpClient = requestJson.createClient(baseMLabURL);
              var query = "f={'id':1,'_id':0}&s={'id':-1}&";

              var idmayor = 0;

              httpClient.get("usuarios?" + query + mLabAPIKey,
                function(err, resMLab, body){
                  var response = !err ? body : {
                    "msg" : "Error obteniendo id usuario mayor"
                  }
                    idmayor = body[0].id + 1;

                    var newUser = {
                      "id": idmayor,
                      "email": req.body.email,
                      "password" : crypt.hash(req.body.password),
                      "nombre": req.body.nombre,
                      "apellido": req.body.apellido
                    }


                    var httpClient = requestJson.createClient(baseMLabURL);


                      httpClient.post("usuarios?" + mLabAPIKey, newUser,
                        function(err, resMLab, body){
                          console.log("Usuario creado en colección usuarios del mongodb(MLab)");

                        res.status(201).send({"msg":"Usuario creado","id":idmayor});
                        }
                      )
                    }
              )

          }

        }
      )
    } else {
      res.status (500).send ({"msg": "Usuario no valido"});
  }

}
//borrar 1 usuario



function deleteUsuario(req, res) {
 console.log("DELETE /apitdm/usuarios/");

 var idUsuario = req.body.id;

 var query = "q={'id':" + idUsuario + "}";
 var httpClient = requestJson.createClient(baseMLabURL);

 httpClient.get("usuarios?" + query + "&"+ mLabAPIKey,
  function(err, resMLab, body){
    var response = !err ? body : {
      "msg" : "Error obteniendo cuentas"
      }

    var findId =false;

    for (var i=0; i < body.length ;i++){

     if(body[i].id==req.body.id){

      var idMongoUser=body[i]._id.$oid;

      findId =true;
      break;
      }
    }

  if (findId == true){

    var queryAccount = "q={'idUsuario':" + idUsuario + "}";
    var httpClient = requestJson.createClient(baseMLabURL);

    httpClient.get("cuentas?" + queryAccount + "&"+ mLabAPIKey,
     function(errAccount, resMLabAccount, bodyAccount){

       var response = !errAccount ? bodyAccount : {
         "msg" : "Error obteniendo cuentas"
         }

       var todascuentas = bodyAccount;

       if (bodyAccount.length > 0){

        var findAccount =false;
        var contador =0;

        for (var i=0; i < bodyAccount.length ;i++){


          var idMongoAccount=bodyAccount[i]._id.$oid;


          var httpClient = requestJson.createClient(baseMLabURL);
          httpClient.delete("cuentas/" + idMongoAccount + "?" + mLabAPIKey,
            function(errDelAccount, resMLabDelAccount, bodyDelAccount) {

              if (errDelAccount) {
                var response = {
                 "msg" : "Error borrando usuario"
              }
               res.status(500).send ({msg:"Error borrando cuenta","IBAN": numAccount});
              } else {

                console.log ("cuenta borrada: " + idMongoAccount);
               }
              }
            )


          var queryMoves='q={"id":' + idUsuario +'}';

          httpClient.get ("movimientos?" + queryMoves + "&" + mLabAPIKey,
              function(errMovimientos,resMLabMovimientos,bodyMovimientos){


                if (bodyMovimientos.length> 0){

                  for (i=0; i < bodyMovimientos.length; i++){

                      var idMongoMoves = bodyMovimientos[i]._id.$oid;

                      var httpClient = requestJson.createClient(baseMLabURL);
                      httpClient.delete("movimientos/" + idMongoMoves + "?" + mLabAPIKey,
                        function(errDelMoves, resMLabDelMoves, bodyDelMoves) {

                            if (errDelMoves) {
                                var response = {
                                  "msg" : "Error borrando movimientos"
                                  }
                                  res.status(500).send ({msg:"Error borrando cuenta","IBAN": numAccount});

                                } else {

                                  console.log ("movimiento borrado");

                              }
                            }
                          )
                        }
                  }
                }
            )
          }

          var httpClient = requestJson.createClient(baseMLabURL);
           httpClient.delete("usuarios/" + idMongoUser + "?" + mLabAPIKey,
             function(errDelUser, resMLabDelUser, bodyDelUser) {
              console.log("err" + resMLabDelUser.statusCode);
             if (errDelUser) {
              var response = {
                             "msg" : "Error borrando usuario"
               }
              res.status(500).send ({msg:"Error borrando usuario","id": idUsuario});
             } else {
              res.status(200).send ({msg:"Usuario Borrado que tiene cuentas","id": idUsuario});
             }
            }
          )


        } else {

          var httpClient = requestJson.createClient(baseMLabURL);
           httpClient.delete("usuarios/" + idMongoUser + "?" + mLabAPIKey,
             function(errDelUser, resMLabDelUser, bodyDelUser) {

             if (errDelUser) {
              var response = {
                             "msg" : "Error borrando usuario"
               }
              res.status(500).send ({msg:"Error borrando usuario","id": idUsuario});
             } else {
               res.status(200).send ({msg:"Usuario Borrado","id": idUsuario});
             }
            }
          )
       }

      }
     )

    } else {
     res.status(500).send ({msg:"No existe el usuario","ID": idUsuario});
   }
  }
 )
}

function changeUsuario(req,res){
  console.log ("POST /apitdm/cambios/");

  var query ="f={'id':1 ,'email':1,'_id':0}&";

  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("usuarios?" + query + "&"+ mLabAPIKey,
   function(err, resMLab, body){
     var response = !err ? body : {
       "msg" : "Error obteniendo email"
       }

     var findEmail =false;

     for (var i=0; i< body.length; i++){

        if(body[i].email==req.body.email) {

          var idUsuario = body[i].id;

          findEmail =true;
          break;
        }
     }


     if (findEmail==true){

       var nombre=req.body.nombre;
       var apellido=req.body.apellido;
       var newNombre =nombre.trim();
       var newApellido =apellido.trim();

       var changeNombre = (newNombre.length == 0)? "igual": "cambioNombre"       ;

       var changeApellido = (newApellido.length == 0)? "igual": "cambioApellido";


       if (changeNombre == "cambioNombre" && changeApellido =="cambioApellido") {

         var newData= '{"$set": {"nombre":' +'"'+ newNombre +'","apellido":'+'"' +newApellido +'"} }';

         var queryData='q={"id":' +idUsuario +'}';

         var httpClient = requestJson.createClient(baseMLabURL);

         httpClient.put("usuarios?" + queryData+ "&" + mLabAPIKey, JSON.parse(newData),
           function(errPutData, resMLabPutData, bodyPutData){
             }
           )

          res.status(201).send({"msg":"Nombre y apellido modificados"});

       } else {

         if (changeNombre == "cambioNombre" && changeApellido =="igual") {

           console.log ("cambio solo nombre");

           var newName= '{"$set": {"nombre":' +'"'+ newNombre +'"} }';

           var queryData='q={"id":' +idUsuario +'}';

           var httpClient = requestJson.createClient(baseMLabURL);

           httpClient.put("usuarios?" + queryData+ "&" + mLabAPIKey, JSON.parse(newName),
             function(errPutName, resMLabPutName, bodyPutName){

               }
             )

            res.status(201).send({"msg":"Nombre modificado"});

         } else {

           if (changeNombre == "igual" && changeApellido =="cambioApellido") {

             var newSurname= '{"$set": {"apellido":' +'"'+ newApellido +'"} }';

             var queryData='q={"id":' +idUsuario +'}';

             var httpClient = requestJson.createClient(baseMLabURL);

             httpClient.put("usuarios?" + queryData+ "&" + mLabAPIKey, JSON.parse(newSurname),
               function(errPutSurname, resMLabPutSurname, bodyPutSurname){

                 }
               )

              res.status(201).send({"msg":"Apellido modificado"});

            }

          if (changeNombre == "igual" && changeApellido =="igual"){
                res.status(200).send ({msg:"Datos permanecen iguales","Nombre": req.body.nombre, "Apellido":req.body.apellido});
          }


         }
       }


     } else {

       res.status(500).send ({msg:"No existe el usuario","eEmail": req.body.email});

     }
   }
 )
}


//referenciar modulos para exponer la función afuera
module.exports.getUsuarios = getUsuarios;
module.exports.postcrearUsuario = postcrearUsuario;
module.exports.getUsuarioById = getUsuarioById;
module.exports.deleteUsuario = deleteUsuario;
module.exports.changeUsuario = changeUsuario;
