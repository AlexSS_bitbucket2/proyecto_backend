const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitdmdb/collections/";

const mLabAPIKey = "apiKey=" + process.env.MLAB_APY_KEY;
const crypt = require('../crypt');
const utils =require('../utils');

function postIngreso (req,res){
  console.log("POST /apitdm/ingresos");

  var cifra =req.body.importe;

  if (utils.validarImporte(cifra)){

    var httpClient = requestJson.createClient(baseMLabURL);
    var numAccount = req.body.IBAN;

    httpClient.get("cuentas?"+ mLabAPIKey,
    function (errAccount, resMLabAccount, bodyAccount){
      var response = !errAccount ? bodyAccount : {
        "msg" : "Error obteniendo cuentas"
        }

      var findAccount =false;

      for (var i=0; i < bodyAccount.length ;i++){


        if(bodyAccount[i].IBAN==req.body.IBAN){

          findAccount =true;
          break;
        }
      }

        if (findAccount==true){

          var idUsuario=bodyAccount[i].idUsuario;

          var balance = bodyAccount[i].balance;

          var result = Number(bodyAccount[i].balance) + Number(req.body.importe);


          var dateOperation = new Date();

          var newOperation = {
           "id": idUsuario,
           "IBAN": numAccount,
           "importe": req.body.importe,
           "operacion": result,
           "fecha": dateOperation
         };


           var httpClient = requestJson.createClient(baseMLabURL);

           httpClient.post("movimientos?" + mLabAPIKey, newOperation,
             function(err, resMLab, body){

                 if (err){
                   var response ={
                   "msg": "Error realizando ingreso"
                   }
                  } else {

                     var newBalance = '{"$set": {"balance":' + newOperation.operacion + '}}';

                       var queryPut='q={"IBAN":' +'"'+ newOperation.IBAN +'"}';

                       var httpClient2 = requestJson.createClient(baseMLabURL);

                       httpClient2.put("cuentas?" + queryPut+ "&" + mLabAPIKey, JSON.parse(newBalance),
                         function(errPut, resMLabPut, bodyPut){
                           }
                         )
                      }

                 res.status(201).send({"msg":"Movimiento realizado, id:" + idUsuario});

               }
             )
            } else {

              res.send({"msg": "Cuenta no existe", "IBAN" :req.body.IBAN});
          }
        }
      )
    } else {

    res.status (500).send ({"msg": "Importe no valido"})
  }
}

//Función sacar dinero
function postReintegro (req,res){
  console.log("POST /apitdm/reintegros");


  var cifra=req.body.importe;

  if (utils.validarImporte(cifra)){

    var httpClient = requestJson.createClient(baseMLabURL);
    var numAccount = req.body.IBAN;

    httpClient.get("cuentas?"+ mLabAPIKey,
    function(errAccount, resMLabAccount, bodyAccount){

      var response = !errAccount ? bodyAccount : {
        "msg" : "Error obteniendo cuentas"
        }

      var findAccount =false;

        for (var i=0; i < bodyAccount.length ;i++){


          if(bodyAccount[i].IBAN==req.body.IBAN){

            findAccount =true;
            break;
          }
        } //fin del for

      if (findAccount==true){

          if (bodyAccount[i].balance < req.body.importe){

              res.send ({"msg": "Saldo insuficiente para realizar la operación"});

            } else {

              var idUsuario=bodyAccount[i].idUsuario;

              var balance =bodyAccount[i].balance;

              var result = bodyAccount[i].balance - req.body.importe;

              var dateOperation = new Date();


          }

            var newOperation = {
             "id": idUsuario,
             "IBAN": numAccount,
             "importe": req.body.importe,
             "operacion": result,
             "fecha": dateOperation
           };


             var httpClient = requestJson.createClient(baseMLabURL);

             httpClient.post("movimientos?" + mLabAPIKey, newOperation,
               function(err, resMLab, body){

                   if (err){
                     var response ={
                     "msg": "Error realizando reintegro"
                     }
                    } else {

                         var newBalance = '{"$set": {"balance":' + newOperation.operacion + '}}';

                         var queryPut='q={"IBAN":' +'"'+ newOperation.IBAN +'"}';

                         var httpClient2 = requestJson.createClient(baseMLabURL);

                         httpClient2.put("cuentas?" + queryPut+ "&" + mLabAPIKey, JSON.parse(newBalance),
                           function(errPut, resMLabPut, bodyPut){

                             }
                           )
                        }


                   res.status(201).send({"msg":"Movimiento realizado, id:" +idUsuario});
                 }

               )


            } else {

                res.send({"msg": "Cuenta no existe", "IBAN" :req.body.IBAN});
          }
        }
      )
    } else {

      res.status (500).send ({"msg": "Importe no valido"})
  }
}


module.exports.postIngreso = postIngreso;
module.exports.postReintegro = postReintegro;
