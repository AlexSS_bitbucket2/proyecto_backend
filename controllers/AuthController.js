const requestJson = require('request-json');
const crypt = require('../crypt');
const utils =require('../utils');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitdmdb/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_APY_KEY;

function postLogin(req, res) {

  console.log("POST /apitdm/login");
  var valor = req.body.email;

  if (utils.validarEmail (valor)){

  console.log(req.body.email);
  console.log(req.body.password);


    var query ="f={'id':1 ,'email':1,'_id':0}&";
    console.log("la query es = " + query);
    var httpClient = requestJson.createClient(baseMLabURL);

    httpClient.get("usuarios?" + query + mLabAPIKey,
      function(errEmail, resMLabEmail, bodyEmail){
       console.log ("Estoy en función email");
       var response = !errEmail ? bodyEmail : {
        "msg" : "Error obteniendo email"
       }

        var findEmail =false;

        for (var i=0; i < bodyEmail.length ;i++){

         if(bodyEmail[i].email==req.body.email){

          console.log ("son iguales");
          findEmail = true;
          }

         }

         if (findEmail == true){
           var query = 'q={"email": "' + req.body.email + '"}';
           console.log("la query es = " + query);

           var putBody = '{"$set":{"logged":true}}';
           var httpClient = requestJson.createClient(baseMLabURL);

           httpClient.get("usuarios?" + query + "&" + mLabAPIKey,
            function(errgetlogin, resMLabgetlogin, bodygetlogin) {
             console.log("dentro función get usuario");

             console.log(req.body.password);
            if (crypt.checkPassword(req.body.password, bodygetlogin[0].password)) {
              httpClient.put("usuarios?" + query + "&" + mLabAPIKey,JSON.parse(putBody),
                function(errputlogin, resMLabputlogin, bodyputlogin) {
                  if (errputlogin) {
                    var response =
                       {
                          "msg" : "Error actualizando usuario"
                       }
                         res.send(response);
                       }  else  {
                            console.log("Usuario logado");
                            res.send({"msg": "Usuario logado.", "id" : bodygetlogin[0].id});
                            console.log(bodygetlogin[0].id);
                        }
                       }
                      )
                     }  else {
                       var response =
                     {
                    "msg" : "Password Incorrecta"
                }
                   res.status (401).send ({"msg": "Password incorrecta"});
                   console.log ("Password incorrecta");
            }
          }
        )
       }  else {
          console.log("Usuario no existe");
          res.status (401).send ({"msg": "Usuario no existe.", "email" :req.body.email});
      }

    }
   )
  } else {
          res.status (500).send ({"msg": "Usuario no valido"});
 }
}


function postLogout (req,res){
  console.log("POST /apittdm/logout/:id");
  var httpClient = requestJson.createClient(baseMLabURL);

  console.log(req.params.id);

  var query = 'q={"id": ' + req.params.id + '}';
  console.log(query);



  httpClient.get("usuarios?" + query + "&"+ mLabAPIKey,
     function(errLogout,resMLabLogout,bodyLogout){
      console.log (bodyLogout);


     var findID =false;
     for (var i=0; i < bodyLogout.length ;i++){
      console.log ("hola " + i);
      console.log (bodyLogout[i].id + "  " + req.params.id);

      if(bodyLogout[i].id==req.params.id){
       console.log ("son iguales");
       console.log (bodyLogout[i].logged);
       findID = true;
      }
     }

     if (findID==true) {
        if(bodyLogout[0].id ==req.params.id && bodyLogout[0].logged){

        console.log ("Usuario existe y está logado");
        var putBody = '{"$unset":{"logged":""}}';

        httpClient.put("usuarios?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
        function(errLogoutp,resMLabLogoutp,bodyLogoutp){

          if (errLogoutp){
            var response = {
              "msg" : "error al borrar logged"
               }
                res.status(500);
               res.send (response);
              } else {
              var response ={

             }
            res.send (response);
            }

           }
          )

         } else {
        console.log ("NO está logado");
        var response ={
          "msg" :  "Usario no está logado"
        }
          res.send (response);

      }
     }
    else {
      console.log("Usuario no existe");
     res.send({"msg": "Usuario no existe.", "id" :req.params.id});
   }
  }
 )
}


module.exports.postLogin = postLogin;
module.exports.postLogout = postLogout;
