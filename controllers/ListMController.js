const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitdmdb/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_APY_KEY;
const crypt = require('../crypt');
const utils =require('../utils');

console.log (mLabAPIKey);
//Listar todas las cuentas dadas de alta
function getOperations (req,res) {

  console.log("GET /apitdm/movimientos/");
  var httpClient = requestJson.createClient (baseMLabURL);

  httpClient.get("movimientos?" + mLabAPIKey,
   function(err, resMLab, body){
     var response = !err ? body : {
       "msg" : "Error obteniendo moviemientos"
     }
     res.send(response);
   }
 )
}



function getOperationsByIBAN(req,res){
  console.log("GET /apitdm/movimientosIban/:iban");
  var httpClient = requestJson.createClient (baseMLabURL);

  var IBAN = req.params.iban;

  console.log (IBAN)
  var query='q={"IBAN":' +'"' + IBAN +'"}';

  console.log (query);

  httpClient.get ("movimientos?" + query + "&" + mLabAPIKey,
      function(err,resMLab,body){

        if (err){
          var response= {
            "msg": "Error obteniendo movimientos"
          }
          res.status(500);
        } else {
          if (body.length > 0){
            var response = body;
          } else {
            var response ={
             "msg": "No hay movimientos de cuentos de esa cuenta"
          }
            res.status(404);
          }
        }
     res.send(response);
      }
  )
}

module.exports.getOperations = getOperations;
module.exports.getOperationsByIBAN=getOperationsByIBAN;
